package com.example.leena.leenaproject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.CountryHolder> {
    List<Model> models;
    Context context;

    public Adapter(List<Model> models, Context context) {
        this.models = models;
        this.context = context;
    }

    @NonNull
    @Override
    public CountryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemLayoutView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycleview_item, null);
        return new CountryHolder(itemLayoutView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull CountryHolder countryHolder, int i) {
        Model model = models.get(i);
        countryHolder.age.setText(Integer.toString(model.age));
        countryHolder.name.setText(model.name);
        countryHolder.image.setImageResource(model.image);
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class CountryHolder extends RecyclerView.ViewHolder {
        TextView age;
        TextView name;
        ImageView image;

        public CountryHolder(@NonNull View itemView) {
            super(itemView);

            age = itemView.findViewById(R.id.PersonAge);
            name = itemView.findViewById(R.id.PersonName);

            image = itemView.findViewById(R.id.PersonImage);
        }
    }
}
