package com.example.leena.leenaproject;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.SubMenu;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    List<Model> personList;
    Adapter adapter;
    int[] ageArray = new int[]{ 19,30,65,43,32,32,43 };
    String[] nameArray = new String[]{"leena","aseel","hale","deema","reema","doaa","lana"};
    int[] imageArray = new int[]{ R.drawable.default_profile,R.drawable.default_profile,R.drawable.default_profile,R.drawable.default_profile,R.drawable.default_profile,R.drawable.default_profile,R.drawable.default_profile };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        personList=new ArrayList<>();

        for(int i=0;i<ageArray.length;i++){
            Model person=new Model();
            person.setName(nameArray[i]);
            person.setAge(ageArray[i]);
            person.setImage(imageArray[i]);
            personList.add(person);
        }

        //Staic way
//        Person person1 = new Person();
//        Person person2 = new Person();
//        person1.setAge(13);
//        person2.setAge(16);
//        person2.setImage(R.drawable.ic_launcher_background);
//        person1.setImage(R.drawable.ic_launcher_background);
//        person1.setName("leena khaled");
//        person2.setName("khaled Qaseem");
//        personList.add(person1);
//        personList.add(person2);
        adapter=new Adapter(personList,this);
        recyclerView.setAdapter(adapter);
        displayMenu();
    }

    private void displayMenu() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        Menu menu = navigationView.getMenu();
//        SubMenu subMenu=menu.addSubMenu("menu1");
//        subMenu.add("1");
//        subMenu.add("2");
        //  navigationView.setNavigationItemSelectedListener(this);
//        menu.add("1");
//        menu.add("1");
//        menu.add("1");
//        menu.add("1");
        drawer.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
